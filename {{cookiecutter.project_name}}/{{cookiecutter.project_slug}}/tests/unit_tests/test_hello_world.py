""" Unit testing example
"""

from {{cookiecutter.project_slug}}.src.main import hello_world
from {{cookiecutter.project_slug}}.utils.loggers import setup_logger

logger, _ = setup_logger("TestHelloWorldLogger", output_folder=False)


def test_hello_world():
    """example of unit test"""
    assert hello_world("something") == "hello world something"

""" Main script
"""
from tqdm import tqdm

from {{cookiecutter.project_slug}}.settings.settings import HELLO_WORLD
from {{cookiecutter.project_slug}}.utils.decorators import timeit
from {{cookiecutter.project_slug}}.utils.loggers import setup_logger

logger, _ = setup_logger("HelloWorldLogger", output_folder=False)


@timeit(colored=True)
def hello_world(something: str) -> str:
    """Add hello world function"""
    logger.info(f"Here's a message from the settings {HELLO_WORLD}")
    return f"hello world {something}"


if __name__ == "__main__":
    hello_world("blabla")

    my_list = list(tqdm(range(10)))
    print(my_list)

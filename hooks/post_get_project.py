import subprocess

def prepare_venv():
    subprocess.check_call(["make", "setup"])

if __name__ == '__main__':
    ...
    
# ds-template

## Description

The `ds-template` is a template used for all my next project. This project aims to unify our way of coding, testing and make it easier for everyone to get started with our projects.
It can be used by Data Scientists to create virtual environnements, link them to a JupyterLab server, and control the code that is being pushed to Gitlab.  


## Requirements
The requirements for the project are the following:  
- python3.6+
- make command
    - For windows users, you can download make command following this [link](https://sourceforge.net/projects/gnuwin32/files/make/3.81/make-3.81.exe/download?use_mirror=netix&download=).For more details on other versions, follow [this page](https://gnuwin32.sourceforge.net/packages/make.htm)
    - For linux/mac users, download make command following your ``sudo apt-get update & apt-get -y install make``   

To check make is correctly installed, type ``make --version``

## Available commands
- ```make init``` will clean the environnement and create a new one with all the requirements installed 
- ```make install``` will add any other library you add in ``requirements.txt`` file.
- ```make safety``` will search for vulnerabilities in your python project
- ```make jupyter-venv-add``` will add your current virtual environnement to a JupyterLab kernel (already installed within your project) 
- ```make jupyter-venv-remove``` will remove your current virtual environnement to a JupyterLab kernel (already installed within your project) 
- ```make clean-logs``` Will clean all unnecessary logs from the project. Only supported on Linux
- ```make precommit-setup``` will setup pre-commit for your current repo. It needs to be a git repository. This command will add all controls before pushing to Gitlab. It's mandatory to activate it.
- ```make setup``` will setup everything for you. Start by running this command on the beginning of every project 


## Dev tools available:

Those command are targeting the **ds-template** folder and the configuration is [here](setup.cfg).

* Code Quality: You can trigger those commands with `make check`.
  * **Formatting** with `black + isort`: To format use ``make format`` and check with `make black` and `make isort` for `black` and `isort` respectively
  * **type-checking** with `mypy`: You can use `make mypy` to check the types and detect errors
  * **Linting** with `flake8 + pylint`: You can use `make flake8` and `make pylint` to lint your code using `flake8` and `pylint` respectively.
* Tests:
  * For testing we use `pytest` and target the tests in the **ds-template** using `make test`
  * You can generate a coverage report using `make coverage` and a html version using `make coverage-html`


# Known issues

This tool is working more accurately on linux, [setup WSL2](https://learn.microsoft.com/fr-fr/windows/wsl/install) on your windows machine.  

- All deletion are done on linux platforms (``clean-logs`` or ``clean-venv``). When you're running on windows, make sure to delete everything manually. 

- Upgrading pip

![](imgs/pip_upgrade.PNG)
When you have issues on upgrading pip, you have 2 options: 
  1. Disable it. Change the parameter **UPGRADE_PIP** you can find in the makefile. Remove the ``wenv`` folder (optionnally), and re-run ``make setup``.  
  2. Do it manually. Run ``.\wenv\Scripts\python.exe -m pip install --upgrade pip``, and then run it again. This step will then be skipped. 

- more to come


